﻿using UnityEngine;
using System.Collections;

public static class Constants  
{	
	public enum TexturesSizes{Small = 32, Medium = 64, Large = 128, XLarge = 256};
	public const float areolaStartRate = 2.0f;
	public const int scoreFoNextLevrelFactor = 2;
	public const int scoreFoNextLevStart = 100;
	public const int scoreSeed = 500;
	public static string urlAreolas = "file://" + Application.dataPath + "/Bundless/Areolas/Areolas.unity3d";
	public static string urlSoundEffect = "file://" + Application.dataPath + "/Bundless/Sounds/AreolasSounds.unity3d";
	public static string urlParticles = "file://" + Application.dataPath + "/Bundless/Particles/particles.unity3d";
	public static string urlBackSounds = "file://" + Application.dataPath + "/Bundless/Sounds/BackgroundSounds.unity3d";
}
