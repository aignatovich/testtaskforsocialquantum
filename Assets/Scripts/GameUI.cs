﻿using UnityEngine;
using System.Collections;

public class GameUI : Singleton<GameUI> {

	
	[SerializeField]
	private UILabel mLblTime;

	[SerializeField] 
	private UILabel mLblScore;

	[SerializeField] 
	private UILabel mLblLevel;

	private int level = 1;

	// Use this for initialization
	void Start () 
	{
		ScoreCounter.Instance.OnNextLevel += OnNextLevel;
	}

	void OnNextLevel()
	{
		level ++;
		mLblLevel.text = "Level " + level;
		Invoke("ResetLevelMark", 3);
	}

	void ResetLevelMark()
	{
		mLblLevel.text = "";
	}

	public void AddScore(int score)
	{
		mLblScore.text = "Score  " + score;
	}
	
	// Update is called once per frame
	void Update () 
	{
		mLblTime.text = ((int)Time.time).ToString();
	}

	public void GoToMenu()
	{
		Application.LoadLevel ("MainMenu");
	}

}
