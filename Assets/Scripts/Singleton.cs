﻿using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	void Awake()
	{
		if ( FindObjectsOfType<T>().Length > 1 )
		{
			DestroyImmediate(FindObjectsOfType<T>()[1].gameObject);
		}
	}

	private static T instance;
	private static object _lock = new object();
	
	public static T Instance
	{
		get
		{
			lock(_lock)
			{
				if (instance == null)
				{
					instance = (T) FindObjectOfType<T>();
					if ( FindObjectsOfType<T>().Length > 1 )
					{
						return instance;
					}
					if (instance == null)
					{
						GameObject singleton = new GameObject();
						instance = singleton.AddComponent<T>();
						singleton.name = "(singleton) "+ typeof(T).ToString();
					}
				}
				return instance;
			}
		}
	}
}

