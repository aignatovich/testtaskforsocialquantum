﻿using UnityEngine;
using System.Collections;
using System;

public class PoolOfTexture : MonoBehaviour{

	Texture2D[] currentTextureArrow;
	Texture2D[] reserveTextureArrow;
	ITextureGenerator  textureGenerator;
	int numberTextureOfConcretType = 4;
	int numberOfTypes = 4; 
	int numberOfAllTexturesInArrow = 16;
	
	void Start()
	{ 	
		ScoreCounter.Instance.SetNumberOftextures(numberOfAllTexturesInArrow);
		ScoreCounter.Instance.OnGenerateTextureTime += GenerateLazzyTextures;
		ScoreCounter.Instance.OnNextLevel += OnNextLevel;
		numberOfAllTexturesInArrow = numberTextureOfConcretType * numberOfTypes;
		currentTextureArrow = new Texture2D[numberOfAllTexturesInArrow];
		reserveTextureArrow = new Texture2D[numberOfAllTexturesInArrow];
		textureGenerator = new GradientTextureGenerator();
		GenerateReserveArrowOfTexture(true);
		StartCoroutine("GenerateReserveArrowOfTexture", true);
	}
	
	public void SetTextureGenerator(ITextureGenerator newGenerator)
	{
		textureGenerator = newGenerator;
	}
	
	void OnNextLevel()
	{
		ChangeArrowOfTexture();
	}
	
	void GenerateLazzyTextures()
	{
		int index;
		index = Array.IndexOf(reserveTextureArrow, null);
		int  targetSize = (int) Mathf.Round( (((float)index) / 4.0f - (index / 4)) * 4.0f);
		if(index != -1)
		{
			reserveTextureArrow[index] = textureGenerator.CreateTexture((int)Mathf.Pow(2, 5 + targetSize ));
		}
	}

	IEnumerator GenerateReserveArrowOfTexture(bool withChangeArrow)
	{
		for(int i = 0; i <  numberTextureOfConcretType; ++i)
		{
			reserveTextureArrow[i] = textureGenerator.CreateTexture((int)Constants.TexturesSizes.Small);
			reserveTextureArrow[i + numberTextureOfConcretType] = textureGenerator.CreateTexture((int)Constants.TexturesSizes.Medium);
			reserveTextureArrow[i + 2 * numberTextureOfConcretType] = textureGenerator.CreateTexture((int)Constants.TexturesSizes.Large);
			reserveTextureArrow[i + 3 * numberTextureOfConcretType] = textureGenerator.CreateTexture((int)Constants.TexturesSizes.XLarge);
		}
		yield return reserveTextureArrow;
		if(withChangeArrow)
		{
			ChangeArrowOfTexture();
		}	
	}
	
	void ChangeArrowOfTexture()
	{
		for( int i = 0; i < numberOfAllTexturesInArrow; ++i)
		{
			currentTextureArrow[i] = reserveTextureArrow[i];	
		}
		System.Array.Clear(reserveTextureArrow, 0, numberOfAllTexturesInArrow);
	}
	
	public Texture2D GetTextureWithSize(Constants.TexturesSizes textureSize)
	{
		int numOfTexture = UnityEngine.Random.Range(0, numberTextureOfConcretType);
		int numberOfReturnedTexture = numOfTexture;
		switch(textureSize)
		{
		case Constants.TexturesSizes.Small:
			break;
		case Constants.TexturesSizes.Medium:
			numberOfReturnedTexture += numberTextureOfConcretType;
			break;
		case Constants.TexturesSizes.Large:
			numberOfReturnedTexture += 2 * numberTextureOfConcretType;
			break;
		case Constants.TexturesSizes.XLarge:
			numberOfReturnedTexture += 3 * numberTextureOfConcretType;
			break;
		}
		return currentTextureArrow[numberOfReturnedTexture];
	}
}
