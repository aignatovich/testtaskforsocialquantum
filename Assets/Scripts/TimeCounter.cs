using UnityEngine;
using System.Collections;

public class TimeCounter : MonoBehaviour {


	Rect pos_For_Time_Label;		
	int currentTime = 0;
	GUIStyle stiyle_for_Time;
	
	void Start () 
	{
		stiyle_for_Time = new GUIStyle();
		stiyle_for_Time.fontSize = 20;
		stiyle_for_Time.fontStyle = FontStyle.BoldAndItalic;
		stiyle_for_Time.normal.textColor = Color.yellow;
		pos_For_Time_Label = new Rect(0.05f * Screen.width, 0.9f * Screen.height, 0.1f * Screen.width, 0.1f * Screen.height);
	}
	
	void FixedUpdate()
	{
		currentTime = (int)Time.time ;
	}
	
	void OnGUI()
	{
		GUI.Label(pos_For_Time_Label, currentTime.ToString(), stiyle_for_Time);
	}
}
