﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Clicker: Singleton<Clicker> 
{
	void Update () 
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit, 1000))
		{
			hit.transform.gameObject.SendMessage("OnClick", SendMessageOptions.DontRequireReceiver);
		}
	}
}