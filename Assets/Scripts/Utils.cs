﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Utils// where T: UnityEngine.Object
{

	public static T RandomItem<T>(List<T> m_items) where T: UnityEngine.Object
	{
		int index = Random.Range(0, m_items.Count);
		return m_items[index];
	}

	public static Color GetRandomColor()
	{
		Color color = Color.white;
		int colorKey = Random.Range(0, 6);
		switch(colorKey)
		{
		case 0:
			color = Color.black;
			break;
		case 1:
			color = Color.blue;
			break;
		case 2:
			color = Color.green;
			break;	
		case 3:
			color = Color.red;
			break;
		case 4:
			color = Color.yellow;
			break;
		case 5:
			color = Color.cyan;
			break;
		case 6:
			color = Color.magenta;
			break;		
		}
		return color;
	}
}
