﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour {
	
	AreolasFactory areolaFactory;
	private float areolaRate = Constants.areolaStartRate ;
	List<AudioClip> backgroundMusic;

	void Start () 
	{
		BundleLoader<AudioClip> backgroundMusicLoader;
		backgroundMusicLoader = new BundleLoader<AudioClip>(Constants.urlBackSounds);
		backgroundMusic = backgroundMusicLoader.LoadedObjects;
		ScoreCounter.Instance.OnNextLevel += OnNextLevel;
		areolaFactory = new AreolasFactory();
		StartGame();
	}

	void StartGame()
	{
		StartBackGroundMusic();
		Invoke("CreateArela", areolaRate);
	}

	void CreateArela()
	{
		Invoke("CreateArela", areolaRate);
		GameObject goAreola =  areolaFactory.GetRandomAreola();
		if(goAreola != null)
			goAreola.SetActive(true);
	}

	void StartBackGroundMusic()
	{
		gameObject.AddComponent<AudioSource>();
		int index = UnityEngine.Random.Range(0, backgroundMusic.Count);
		audio.clip = backgroundMusic[index];
		audio.loop = true;
		audio.Play();
	}

	void OnNextLevel()
	{
		areolaRate /= Global.Instance.AreolaRateDivider;
	}
}
