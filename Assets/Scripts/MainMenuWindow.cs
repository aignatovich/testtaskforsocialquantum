﻿using UnityEngine;
using System.Collections;

public class MainMenuWindow : MonoBehaviour {

	[SerializeField]
	private UIButton mBtnStart;

	[SerializeField]
	private UIPopupList mPpLtDifficult;

	[SerializeField]
	private UIScrollView mScrlVwDifficult;

	[SerializeField]
	private UICenterOnChild mCenterOnChild;

	// Use this for initialization
	void Start () 
	{
		EventDelegate.Add (mBtnStart.onClick, OnClick);
		EventDelegate.Add (mPpLtDifficult.onChange, SetDifficult);
		//mScrlVwDifficult.onDragFinished += OnDragFinished;
		mCenterOnChild.onFinished += OnDragFinished;
		if (Global.Instance.difficult != 0) 
		{
			mPpLtDifficult.value = Global.Instance.difficult.ToString();
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	} 

	void OnDragFinished()
	{
		string selectedName = mCenterOnChild.centeredObject.name;
		switch(selectedName)
		{
			case "SpriteEasy":
			mPpLtDifficult.value = "easy";
			mPpLtDifficult.value = "easy";
			break;

			case "SpriteNormal":
			mPpLtDifficult.value = "medium";
			mPpLtDifficult.value = "medium";
			break;

			case "SpriteHard":
			mPpLtDifficult.value = "hard";
			break;
	
		}
		SetDifficult ();
	}
	 
	void SetDifficult()
	{
		switch (mPpLtDifficult.value) 
		{
		case "easy":
			Global.Instance.difficult = Difficult.easy;
			break;
		case "medium":
			Global.Instance.difficult = Difficult.medium;
			break;
		case "hard":
			Global.Instance.difficult = Difficult.hard;
			break;
		}

	}

	void OnClick()
	{
		Application.LoadLevel ("GameScene");
	}
}
