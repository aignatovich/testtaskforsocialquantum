﻿using UnityEngine;
using System.Collections;

public class ScoreCounter : Singleton<ScoreCounter> {
	
	public delegate void OnScoreCounterDelegate();
	public event OnScoreCounterDelegate OnNextLevel;
	public event OnScoreCounterDelegate OnGenerateTextureTime;
	private  static ScoreCounter instance;   
	int currentScore ;
	private int scoreForNextLevel = Constants.scoreFoNextLevStart;
	private int textureGenerateInterval = int.MaxValue;
	private int textureGenerateCounter = int.MinValue;

	public void SetNumberOftextures(int numOfTextures)
	{
		textureGenerateInterval = ( scoreForNextLevel - currentScore ) / (numOfTextures + 1);
		textureGenerateCounter = 0;
	}

	void Start () 
	{
		currentScore = 0;
	}
	
	void Update () 
	{
		 GameUI.Instance.AddScore (currentScore);
	}

	public void  AddScore(int score)
	{
		textureGenerateCounter += score;
		while(textureGenerateCounter >= textureGenerateInterval)
		{
			textureGenerateCounter -= textureGenerateInterval;
			OnGenerateTextureTime();
		}

		currentScore += score;
		if (currentScore > scoreForNextLevel)
		{
			OnNextLevel();
			scoreForNextLevel *= Constants.scoreFoNextLevrelFactor;
		}
	}
	
	public int GetScore()
	{
		return currentScore;
	}
}