﻿using UnityEngine;
using System.Collections;

public class Global : Singleton<Global> {

	public float speedSeedMultiplicator = 1.45f;
	public float AreolaRateDivider = 1.25f;
	private Difficult mDifficult;


	public Difficult difficult{
		get
		{
			return mDifficult;
		}
		set
		{
			mDifficult = value;
			switch(value)
			{
			case Difficult.easy:
				speedSeedMultiplicator = 1.1f;
				AreolaRateDivider = 1.1f;
				break;

			case Difficult.hard:
				speedSeedMultiplicator = 1.8f;
				AreolaRateDivider = 1.5f;
				break;
			}
		}
	}

	// Use this for initialization
	void Start () 
	{
		DontDestroyOnLoad (gameObject);	
	}
}

public enum Difficult
{
	easy, medium, hard
}