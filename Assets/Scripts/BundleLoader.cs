﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

 class BundleLoader<T> where T: UnityEngine.Object
{
	public delegate void OnAreolasBundleLoadedEvent();
	public event OnAreolasBundleLoadedEvent OnAreolasBundleLoaded;

	public BundleLoader(string bundleURL)
	{
		LoadedObjects = LoadBundleObjects(bundleURL);
	}

	public List<T> LoadedObjects
	{
		get; private set;
	}

	List<T> LoadBundleObjects(string urlToBundles) 
	{
		List<T> loadedObjects =  new List<T>();
		WWW www = WWW.LoadFromCacheOrDownload (urlToBundles, 1);
		AssetBundle bundle = www.assetBundle;
		UnityEngine.Object[] loadedTempObjects = bundle.LoadAll();
		for(int i = 0; i <  loadedTempObjects.Length; ++i)
		{
			if( !(loadedTempObjects[i] is Component))
			{
				loadedObjects.Add((T)GameObject.Instantiate(loadedTempObjects[i]));
			}
		}
		bundle.Unload(false);
		www.Dispose();
		return loadedObjects;
	}
}
