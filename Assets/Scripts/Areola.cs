using UnityEngine;
using System.Collections;

public class Areola : MonoBehaviour {

	public AreolasFactory areolasFactory;
	private GameObject childParticleSystem = null;

	void OnClick()
	{
		ScoreCounter.Instance.AddScore((int)(  (Constants.scoreSeed / transform.lossyScale.x) ));
		ReturnParticles();
		childParticleSystem.GetComponent<ParticleSystem>().Emit((int)(transform.lossyScale.x * 3));
		childParticleSystem.transform.parent = null;
		childParticleSystem.audio.Play();
		ReturnThisObgectInPool();
	}

	private void ReturnParticles()
	{
		childParticleSystem.transform.parent = gameObject.transform;
		childParticleSystem.transform.Translate(transform.position - childParticleSystem.transform.position);
	}
	
	public void AddParticleSystem(UnityEngine.Object newParticleSystem)
	{
		if(childParticleSystem != null)
		{
			return;
		}
		childParticleSystem = (GameObject)GameObject.Instantiate( newParticleSystem, this.transform.position, Quaternion.identity);
		childParticleSystem.transform.parent = gameObject.transform;
	}
	
	public void AddSoundEffect(AudioClip clip)
	{
		if(childParticleSystem.audio != null)
		{
			return;
		}
		childParticleSystem.AddComponent<AudioSource>();
		childParticleSystem.audio.clip = clip ;
		childParticleSystem.audio.playOnAwake = false;
	}

	void OnTriggerEnter(Collider coll)
	{
		if(coll.name == "DeathAreolaTrigger")
		{
			ReturnThisObgectInPool();
		}
	}

	void ReturnThisObgectInPool()
	{
		areolasFactory.ReturnAreolasInPool(this.gameObject);
	}
}
