﻿using UnityEngine;
using System.Collections;

public class TextureGenerator : ITextureGenerator
{
	public Texture2D CreateTexture (int sizeOfTexture)
	{	
		Texture2D tex =  new Texture2D(sizeOfTexture, sizeOfTexture);
		Color32[] colors = new Color32[sizeOfTexture* sizeOfTexture];
		for(int i = 0; i < sizeOfTexture* sizeOfTexture; ++i)
		{
			colors[i] = new Color32(2,25,125,0);
		}
		tex.SetPixels32(colors);
		tex.Apply();
		return tex;
	}
}
