﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AreolasFactory
{

	private List<UnityEngine.Object>	Particles;
	private List<AudioClip>	SoundsEffect;
	private List<GameObject> areolasArray;
	private Vector3 storagePositionForAreolas;
	private  static AreolasFactory instance;
	private PoolOfTexture poolOfTexture;

	Camera camera;
	float speedSeed = -200;

	public  AreolasFactory()
	{	

		BundleLoader<GameObject> AreolasArrayBundle;
		BundleLoader<AudioClip> SoundsEffectBundle ;
		BundleLoader<UnityEngine.Object> ParticlesBundle;

		AreolasArrayBundle = new BundleLoader<GameObject>(Constants.urlAreolas);
		SoundsEffectBundle = new BundleLoader<AudioClip>(Constants.urlSoundEffect);
		ParticlesBundle = new BundleLoader<Object>(Constants.urlParticles);

		SoundsEffect = SoundsEffectBundle.LoadedObjects;
		Particles = ParticlesBundle.LoadedObjects;

		ScoreCounter.Instance.OnNextLevel += OnNeextLevel;
		camera = Camera.main;
		areolasArray = new List<GameObject>();
	
		areolasArray = AreolasArrayBundle.LoadedObjects;
		storagePositionForAreolas = new Vector3(1000, 1000, 100);
		foreach(GameObject areola in areolasArray)
		{
			areola.AddComponent<Areola>().areolasFactory = this;
			areola.SetActive(false);
		}
		poolOfTexture = (PoolOfTexture)GameObject.FindObjectOfType(typeof(  PoolOfTexture)) ;
	}

	void OnNeextLevel()
	{
		speedSeed *= Global.Instance.speedSeedMultiplicator;
	}
	
	public GameObject GetRandomAreola()
	{
		GameObject tempRefForAreola = null;
		int AreolaIndex = Random.Range(0, areolasArray.Count -1);
		tempRefForAreola = areolasArray[AreolaIndex];
		while(tempRefForAreola.activeSelf)
		{
			AreolaIndex ++;
			if(AreolaIndex > areolasArray.Count - 1)
			{
				return null;
			}
			tempRefForAreola = areolasArray[AreolaIndex];
		}
		tempRefForAreola.transform.Translate(- tempRefForAreola.transform.position);
		SetAreolaParametres( tempRefForAreola);
		return tempRefForAreola;
	}

	
	Constants.TexturesSizes SetSize( ref GameObject areolaObject)
	{
		int size = UnityEngine.Random.Range(1, 40);
		int sizeOfTexture = (int)(size / 10);
		Constants.TexturesSizes textureSize = Constants.TexturesSizes.Large ;
		switch(sizeOfTexture)
		{
		case 1:
			textureSize = Constants.TexturesSizes.Small;
			break;
		case 2:
			textureSize = Constants.TexturesSizes.Medium;
			break;
		case 3:
			textureSize = Constants.TexturesSizes.Large;
			break;
		case 4:
			textureSize = Constants.TexturesSizes.XLarge;
			break;
		}
		size += 5;
		areolaObject.transform.localScale = new Vector3(size, size, size);
		return textureSize;
	}

	private GameObject SetAreolaParametres(GameObject areolaObject)
	{
		Constants.TexturesSizes textureSize = SetSize(ref areolaObject);
		areolaObject.renderer.material.mainTexture = poolOfTexture.GetTextureWithSize(textureSize);
	//	Areola refAreola = areolaObject.AddComponent<Areola>();
		Areola refAreola = areolaObject.GetComponent<Areola>();
		refAreola.AddParticleSystem(Utils.RandomItem( Particles));
		refAreola.AddSoundEffect(Utils.RandomItem(SoundsEffect));
		areolaObject = SetTransform(areolaObject);
		areolaObject = SetPhysics(areolaObject);
		return areolaObject;
	}

	private GameObject SetTransform(GameObject areolaObject)
	{
		float radiusOfAreola = 0.5f * areolaObject.transform.localScale.x;
		Vector3 startPointForAreola = new Vector3();
		startPointForAreola.y = Camera.main.orthographicSize - radiusOfAreola;
		startPointForAreola.x = Random.Range(-camera.aspect * camera.orthographicSize + radiusOfAreola, camera.aspect * camera.orthographicSize - radiusOfAreola);
		startPointForAreola.z += 30;	
		areolaObject.transform.position = startPointForAreola;
		return areolaObject;
	}

	private GameObject SetPhysics(GameObject areolaObject)
	{
		Rigidbody areolasRigidbody = areolaObject.rigidbody;
		if(areolasRigidbody == null)
		{
			areolasRigidbody = areolaObject.AddComponent( typeof( Rigidbody)) as Rigidbody;
			
		}
		areolasRigidbody.velocity = new Vector3(0, speedSeed / areolaObject.transform.localScale.x);
		return areolaObject;
	}

	public void ReturnAreolasInPool(GameObject areolasObject )
	{
		areolasObject.SetActive(false);
		areolasObject.transform.Translate(storagePositionForAreolas - areolasObject.transform.position);
	}
}
