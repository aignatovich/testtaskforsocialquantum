﻿using UnityEngine;
using System.Collections;

public class GradientTextureGenerator : ITextureGenerator
{
	public Texture2D CreateTexture (int sizeOfTexture)
	{
		Gradient gradient;
		GradientColorKey[] gradientColorKey;
		GradientAlphaKey[] gradientAlphaKey;
		gradient = new Gradient();
		gradientColorKey = new GradientColorKey[2];
		gradientColorKey[0].color = Utils.GetRandomColor();
		gradientColorKey[0].time = 0.0f;
		gradientColorKey[1].color = Utils.GetRandomColor();
		gradientColorKey[1].time = 1;
		gradientAlphaKey = new GradientAlphaKey[2];
		gradientAlphaKey[0].alpha = 1.0f;
		gradientAlphaKey[0].time = 0.0f;
		gradientAlphaKey[1].alpha = 0.0f;
		gradientAlphaKey[1].time = 1;
		gradient.SetKeys(gradientColorKey, gradientAlphaKey);
		Texture2D texture =  new Texture2D(sizeOfTexture, sizeOfTexture);
		Color32[] colors = new Color32[sizeOfTexture * sizeOfTexture];
		for(int i = 0; i < sizeOfTexture; ++i)
		{
			for(int j = 0; j < sizeOfTexture; ++j)
			{
				colors[i*sizeOfTexture + j] = gradient.Evaluate((float)i/sizeOfTexture);
			}
		}
		texture.SetPixels32(colors);
		texture.Apply();
		return texture;
	}
}