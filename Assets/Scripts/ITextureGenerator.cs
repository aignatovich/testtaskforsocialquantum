﻿using UnityEngine;
using System.Collections;

public interface ITextureGenerator 
{
	Texture2D CreateTexture (int sizeOfTexture);
}

